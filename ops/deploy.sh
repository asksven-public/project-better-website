#!/usr/bin/env bash


# To test this locally
# 1. install jinja2
# 2. set env vars using "source ./setenv.sh"
#   $NAMESPACE the namespace to deploy to
#   $DEPLOY_ENV is either "production", or "development"
#   $CI_COMMIT_SHA is the sha of the build, e.g. 1ecfd275763eff1d6b4844ea3168962458c9f27a and is used to tag the image
#   $CI_COMMIT_REF_SLUG is the branch name

echo '---$DEPLOY_ENV:' ${DEPLOY_ENV}
echo '---$NAMESPACE:' ${NAMESPACE}
echo '---$CI_COMMIT_REF_SLUG:' ${CI_COMMIT_REF_SLUG}

if [ "$DEPLOY_ENV" = "production" ]
then
    HOST1="better.asksven.io"
elif [ "$DEPLOY_ENV" = "temp" ]
then
    # for temp we use the ${CI_COMMIT_SHA} to create a unique URL    
    HOST1="better-${CI_COMMIT_SHA}.asksven.io"
elif [ "$DEPLOY_ENV" = "dr" ] 
then
    # for homelab we use the a static name    
    HOST1="better.dr.asksven.io"    
else
    # we use the branch name in the URL
    HOST1="better-${CI_COMMIT_REF_SLUG}.asksven.io"
fi
echo '---$HOST1:' ${HOST1}


# $NAMESPACE is defined in .gitlab-ci.yaml

kubectl create namespace $NAMESPACE

# Label temp and testing namespaces with TTL 1d
if [ "$DEPLOY_ENV" = "temp" ]
then
    kubectl annotate namespace $NAMESPACE janitor/ttl=1d
fi

K8S_DIR=./manifests
TARGET_DIR=${K8S_DIR}/.generated

rm -rf ${TARGET_DIR}/*
mkdir -p ${TARGET_DIR}/values


for f in ${K8S_DIR}/*.yaml
do
  echo processing $f
  jinja2 $f --format=yaml --strict -D TAG=${CI_COMMIT_SHA} -D HOST1=${HOST1} -D DEPLOY_ENV=${DEPLOY_ENV} > "${TARGET_DIR}/$(basename ${f})"
done



kubectl --namespace=$NAMESPACE apply -f ${TARGET_DIR}


